<?php

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

use CliText\Base as CT;
use CliText\RowSettings;

CT::lb();
/*
CT::label(' Coinbase Pro tool ', TS::BOLD, FC::RED, BC::UNDERLINED);

CT::lb(2);

CT::tab("Line 1\nLine 2");

CT::lb(2);
*/

/*
CT::tab(
    CT::row(
        [
            CT::column('Test me'),
            CT::column('Test me 2'),
        ], 10, 0
    )
);
*/

// @todo ENV COLORED_ENVIRONMENT = bool ???

$RowSettings = (new RowSettings())
    ->columnLength(50)
    ->columnsSeparatedBy(CT::_label('| ') . '$ ');

$RowSettingsHeader = clone $RowSettings;

$RowSettingsHeader
    ->borderTopString(' ')
    ->topBorderMarginLeft(0)
    ->timesToExtendTopBorder(3);

CT::tab(
    CT::_row(
        [
            'Test me 1',//CT::_label('Test me'),
            'Test me 2',
            'Test me 3',
            CT::_label('Test me')
        ],
        $RowSettingsHeader
    ) .
    CT::_row(
        [
            'Test me 2.1',
            'Test me 2.2',
            'Test me 2.3',
            '4'
        ],
        $RowSettings
    )
);

CT::lb();

CT::tab(
    CT::_row(
        array_pad(['1'], 4, ''),
        $RowSettingsHeader
    )
);

CT::lb(2);


/*
CT::tab(
    CT::_row(
        [
            'Test me',
            'Test me 2',
            'Test me 3',
        ],
        20, "  "
    )
);

CT::lb(2);

CT::tab(
    CT::_tab(
        CT::_row(
            [

                'Test me',
                'Test me 2',
                'Test me 3',
            ],
            10, "  "
        )
    )
);

CT::lb(2);
*/