<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use CliText\Base as CT;


echo CT::linebreak();

CT::tab(CT::label(' ETH trading strategy '));

echo CT::linebreak(2);

CT::tab(
    CT::rowOld([
        CT::columnOld(CT::label(' TRADE ', '41m') . " bank = 10 pots (p)
", true),

        CT::columnOld(CT::label(' INVEST ', '41m')  . " bank = 10 pots (p)
", true),

        CT::columnOld("
1 pot = 500 eur
", true),

        CT::columnOld("
Range: " . CT::label(' TRADE ', '41m') . " 110 - 200, " . CT::label(' INVEST ', '41m') ." 85 - 160
", true),

    ], true) .

    CT::line(4, true) .

    CT::rowOld([

        CT::columnOld(CT::label(' TRADING Strategy 1 ', '42m') . ", 3 pots
110, 143.33, 176.66, 209.99

176.66: 1p, 187.77: 0.5p, 198.88: 0.23p
143.33:     154.44:       165.55:
110:        121.11:       132.22:
", true),

        CT::columnOld(CT::label(' TRADING Strategy 2 ', '42m') . ", 3 pots
110, 143.33, 176.66, 209.99

190: 0.3p
180: 0.3p
170: 0.4p *1
...
140: 0.7p *2
...
110:   1p *3

" . CT::label(' TOTAL (moving down): ', '41m') . "
170: 0.4p
140: 0.7p + 0.1p
110:   1p + 0.1p + 0.1p
", true),

        CT::columnOld(CT::label(' TRADING Strategy 3 ', '42m') . ", 2 pots
110, 120, ..., 200

200: 0.2p
190: 0.2p
...
110: 0.2p
", true),

        CT::columnOld(CT::label(' TRADING Strategy 4 ', '42m') . ", 2 pots
110, 143.33, 176.66, 209.99

176.66: 0.66p
143.33: 0.66p
110:    0.66p
", true),


    ], true) .

    CT::line(4, true) .


    CT::linebreak(2) .


    CT::label(" TRADE INVESTMENTS ") . CT::linebreak() . "When price goes higher than trading range " .

    CT::linebreak(2) .

    CT::rowOld([

        CT::columnOld(
            CT::label(" Strategy 1 ", '41m') . "
198.88: 0.23p
165.55: 0.23p 
132.22: 0.23p
", true),


        CT::columnOld(
            CT::label(" Strategy 2 ", '41m') . "
190: 0.3p * 3
", true),


        CT::columnOld(
            CT::label(" Strategy 3 ", '41m') . "
200: 0.2p
", true),

    ], true) .

    CT::label(" TOTAL ", '41m') . " 1.79p" .

    CT::linebreak(3) .

    CT::label(" INVESTING STRATEGY ", '42m') . CT::linebreak() .

    "85, 110, 135, 160"
);

echo CT::linebreak();