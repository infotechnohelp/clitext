<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use CliText\Base as CT;

CT::line();



echo CT::columnOld("
Bank      = 20 pots (p)
TRADING   = 8p
INVESTING = 12p
");

CT::line(3);

CT::rowOld([

    CT::columnOld("
TRADING Strategy 1, 3 pots
110, 143.33, 176.66, 209.99

176.66: 1p, 187.77: 0.5p, 198.88: 0.23p
143.33:     154.44:       165.55:
110:        121.11:       132.22:
", true),

    CT::columnOld("
TRADING Strategy 2, 3 pots
110, 143.33, 176.66, 209.99

120, 130, ..., 200

190: 0.3p
180: 0.3p
170: 0.4p *1
...
140: 0.7p *2
...
110:   1p *3
[r
[lbl TOTAL (moving down):
[lbl:green ' 170 ' l]: [c]0.4p
140: 0.7p + [c'0.1p

'c]
110:   1p + 0.1p + 0.1p
// [r3(gap) [c '110 :'  [c 1*p [c + 0.1p [c + 0.1p r]
// 
r]

[tbl3(gap):" . CT::label(" no need ", "41m") . "3(columns)
1a;1b;1c

tbl]

[ln

", true),

    CT::columnOld("
TRADING Strategy 3, 2 pots

110, 120, ..., 200

200: 0.2p
190: 0.4p
...
110:   2p
", true),

]);

CT::columnOld(
    " " . CT::label(" When trading range is off: ") . "
\t" . CT::label(" Strategy 1 ", '41m') . "
[tb(tab)
198.88: 0.23*p
165.55: 0.23*p " . CT::label(" twig ", '41m') ." {{ 0.23 * p(pot) }} 
132.22: 0.23*p
t]

[tb] [l Strategy 2
190: 0.3p * 3

Strategy 3
200(90%r". CT::label(" {{ 0.9 * r(range) }} ")."): 0.2p

" . CT::label("Close on RANGE TOP * 2 (420)"));



echo "\n\n";
echo " 1 Done\r";
sleep(1);
echo " 2 Done\r";
sleep(1);
echo " 3 Done\n";



echo "\n\n";
echo "  " . CT::label(' try ') ."  " . CT::label(' ', '41m')." \r";
sleep(1);
echo "  " . CT::label(' try me ') ."  " . CT::label('  ', '41m')." \r";
sleep(1);
echo "  " . CT::label(' try me please ') ." " . CT::label('   ', '41m')." \r";
sleep(1);



echo "\n\n";

echo "  " . CT::label(' ', '40m') . CT::label('     ', '41m')." \r";
sleep(1);
echo "  " . CT::label('  ', '40m') . CT::label('    ', '41m')." \r";
sleep(1);
echo "  " . CT::label('   ', '40m')  . CT::label('   ', '41m')." \r";
sleep(1);
echo "  " . CT::label('    ', '40m') . CT::label('  ', '41m')." \r";
sleep(1);
echo "  " . CT::label('     ', '40m')  . CT::label(' ', '41m') . "\r";
sleep(1);
echo "  " . CT::label('      ', '40m')  . CT::label('', '41m') . "\n";
echo "\n";
sleep(1);

echo "  " . CT::label(' try ') ."  " . CT::label(' ', '41m')." \r";
sleep(1);
echo "  " . CT::label(' try me ') ."  " . CT::label('  ', '41m')." \r";
sleep(1);
echo "  " . CT::label(' try me please ') ." " . CT::label('   ', '41m');

echo " Common!\n\n\n";