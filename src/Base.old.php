<?php

namespace CliText;

use CliText\BackgroundColors as BC;
use CliText\ForegroundColors as FC;
use CliText\TextStyles as TS;

class Base
{
    public static function line($columnAmount = 1, $return = false, $gapLength = 3, $columnLength = 48)
    {
        $result = "";

        for ($i = 0; $i < $columnAmount; $i++) {
            $result .= str_pad("", $columnLength, "_") .
                str_pad("", $gapLength, " ");
        }

        $result .= "\n";

        if ($return) {
            return "$result";
        }

        echo "$result";
    }


    public static function columnOld($msg, $padLength = 30)
    {
        $msg = trim($msg, "\n");

        $result = "";

        foreach (explode("\n", $msg) as $line) {

            if (strpos($line, '[0m') !== false) {
                $padLength = $padLength;
            }

            $result .= str_pad($line, $padLength, " "/*, STR_PAD_LEFT*/) . "\n";

            if (strpos($line, '[0m') !== false) {
                $padLength = $padLength;
            }
        }

        return $result;
    }


    public static function rowOld(array $columns, $columnLength = 10, $gapLength = 3)
    {
        $columnsAndLines = [];
        $i = 0;

        foreach ($columns as $column) {
            $columnsAndLines[$i] = explode("\n", $column);
            $i++;
        }

        $maxColumnHeight = 0;

        foreach ($columnsAndLines as $columnLines) {
            $maxColumnHeight = (count($columnLines) > $maxColumnHeight) ? count($columnLines) : $maxColumnHeight;
        }

        $result = "";

        for ($i = 0; $i < $maxColumnHeight; $i++) {

            for ($i2 = 0; $i2 < count($columnsAndLines); $i2++) {
                if (!isset($columnsAndLines[$i2][$i]) || empty($columnsAndLines[$i2][$i])) {
                    $result .= str_pad("", $columnLength, " ") . str_pad("", $gapLength, " ");
                    var_dump($result);
                    continue;
                }

                $result .= $columnsAndLines[$i2][$i] . str_pad("", $gapLength, " ");

                var_dump($result);
            }

            $result .= "\n";

        }

        return $result;
    }


    public static function _row(
        array $columns,
        $columnLength = 10, $separetedBy = null, $borderTop = null, int $extendBorderTopWith = 0,int $topBorderMarginLeft = 0
    )
    {
        return self::row($columns, $columnLength, $separetedBy, $borderTop, $extendBorderTopWith, $topBorderMarginLeft, true);
    }

    public static function row(
        array $columns,
        $columnLength = 10, $separetedBy = null,
        $borderTop = null, int $extendTopBorderWith = 0, int $topBorderMarginLeft = 0,
        bool $return = false
    )
    {
        $columnsAndLines = [];
        $i = 0;

        foreach ($columns as $column) {
            $columnsAndLines[$i] = explode("\n", $column);
            $i++;
        }

        $maxColumnHeight = 0;

        foreach ($columnsAndLines as $columnLines) {
            $maxColumnHeight = (count($columnLines) > $maxColumnHeight) ? count($columnLines) : $maxColumnHeight;
        }

        $result =
            str_repeat(' ', $topBorderMarginLeft) .
            str_repeat($borderTop, ($columnLength + strlen($separetedBy)) * count($columns) + $extendTopBorderWith - $topBorderMarginLeft) .
            "\n";

        for ($i = 0; $i < $maxColumnHeight; $i++) {

            for ($i2 = 0; $i2 < count($columnsAndLines); $i2++) {
                if (!isset($columnsAndLines[$i2][$i]) || empty($columnsAndLines[$i2][$i])) {
                    $result .= str_pad("", $columnLength, " ");
                    continue;
                }

                if ($i2 === 0) {
                    $result .= $separetedBy;
                }

                $result .= str_pad($columnsAndLines[$i2][$i], $columnLength, " ") . $separetedBy;
            }

        }

        return self::processResult($result, $return);
    }


//https://joshtronic.com/2013/09/02/how-to-use-colors-in-command-line-output/

    private static function processResult(string $result, bool $return)
    {
        if ($return) {
            return $result;
        }

        echo $result;
    }

    public static function label(
        string $msg,
        $style = TS::BOLD, $foregroundColor = FC::GREEN, $backgroundColor = BC::UNDERLINED,
        bool $return = false
    )
    {
        self::processResult("\e[$style;$foregroundColor;{$backgroundColor}m$msg\e[0m", $return);
    }


    public static function _tab($msg)
    {
        return self::tab($msg, true);
    }

    public static function tab($msg, $return = false)
    {
        $result = [];

        foreach (explode("\n", $msg) as $line) {
            $result[] = "\t$line";
        }

        return self::processResult(implode("\n", $result), $return);
    }


    public static function filled()
    {

    }

    public static function blankLine()
    {

    }


    public static function spaced($msg)
    {
        $result = [];

        foreach (explode("\n", $msg) as $line) {
            $result[] = " $line";
        }

        echo implode("\n", $result);
    }

    public static function linebreak(int $times = 1)
    {
        self::processResult(str_repeat("\n", $times), false);
    }

    public static function lb(int $times = 1)
    {
        self::linebreak($times);
    }


    public static function lpad($msg, $length, $str = " ")
    {
        return str_pad($msg, $length, $str, STR_PAD_LEFT);
    }
}