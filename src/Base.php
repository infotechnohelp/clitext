<?php

namespace CliText;

use CliText\BackgroundColors as BC;
use CliText\ForegroundColors as FC;
use CliText\TextStyles as TS;

class Base
{
    public static function line($columnAmount = 1, $return = false, $gapLength = 3, $columnLength = 48)
    {
        $result = "";

        for ($i = 0; $i < $columnAmount; $i++) {
            $result .= str_pad("", $columnLength, "_") .
                str_pad("", $gapLength, " ");
        }

        $result .= "\n";

        if ($return) {
            return "$result";
        }

        echo "$result";
    }


    public static function columnOld($msg, $padLength = 30)
    {
        $msg = trim($msg, "\n");

        $result = "";

        foreach (explode("\n", $msg) as $line) {

            if (strpos($line, '[0m') !== false) {
                $padLength = $padLength;
            }

            $result .= str_pad($line, $padLength, " "/*, STR_PAD_LEFT*/) . "\n";

            if (strpos($line, '[0m') !== false) {
                $padLength = $padLength;
            }
        }

        return $result;
    }


    public static function rowOld(array $columns, $columnLength = 10, $gapLength = 3)
    {
        $columnsAndLines = [];
        $i = 0;

        foreach ($columns as $column) {
            $columnsAndLines[$i] = explode("\n", $column);
            $i++;
        }

        $maxColumnHeight = 0;

        foreach ($columnsAndLines as $columnLines) {
            $maxColumnHeight = (count($columnLines) > $maxColumnHeight) ? count($columnLines) : $maxColumnHeight;
        }

        $result = "";

        for ($i = 0; $i < $maxColumnHeight; $i++) {

            for ($i2 = 0; $i2 < count($columnsAndLines); $i2++) {
                if (!isset($columnsAndLines[$i2][$i]) || empty($columnsAndLines[$i2][$i])) {
                    $result .= str_pad("", $columnLength, " ") . str_pad("", $gapLength, " ");
                    //var_dump($result);
                    continue;
                }

                $result .= $columnsAndLines[$i2][$i] . str_pad("", $gapLength, " ");

                //var_dump($result);
            }

            $result .= "\n";

        }

        return $result;
    }


    public static function _row(
        array $columns,
        RowSettings $settings = null
    )
    {
        return self::row($columns, $settings, true);
    }

    public static function row(
        array $columns,
        RowSettings $s = null,
        bool $return = false
    )
    {
        $s = $s ?? new RowSettings();

        $columnsAndLines = [];
        $i = 0;

        foreach ($columns as $column) {
            $columnsAndLines[$i] = explode("\n", $column);
            $i++;
        }

        $maxColumnHeight = 0;

        foreach ($columnsAndLines as $columnLines) {
            $maxColumnHeight = (count($columnLines) > $maxColumnHeight) ? count($columnLines) : $maxColumnHeight;
        }

        $columnLength = $s->getColumnLength();
        $separetedBy = $s->getColumnsSeparatedBy();
        $extendTopBorderWith = $s->getTimesToExtendTopBorder();
        $topBorderMarginLeft = $s->getTopBorderMarginLeft();
        $borderTopString = $s->getBorderTopString();

        if (strlen($borderTopString) > 1) {
            // @todo Update in one of next releases
            throw new \Exception("Border top string not longer than 1 character ($borderTopString)");
        }

        $strLenSeparatedBy = strlen($separetedBy);

        if (strpos($separetedBy, "\e[0m") !== false) {
            $line = str_replace("\e[", '', $separetedBy);

            $line = str_replace("0m", '', $line);

            $explodedLine = explode('m', $line);

            array_shift($explodedLine);

            $line = implode($explodedLine, 'm');

            $strLenSeparatedBy =  strlen($line);
            
        }
        
        $result =
            str_repeat(' ', $topBorderMarginLeft) .
            str_repeat(
                $borderTopString,
                ($columnLength + $strLenSeparatedBy) * count($columns) + $extendTopBorderWith - $topBorderMarginLeft
            );

        //$result = self::_label($result, TS::BOLD, FC::WHITE, BC::GREEN) ."\n";

        $result = self::_label($result, TS::UNDERLINE, FC::WHITE) . "\n";

        $result .= "\n";

        for ($i = 0; $i < $maxColumnHeight; $i++) {

            for ($i2 = 0; $i2 < count($columnsAndLines); $i2++) {


                if (!isset($columnsAndLines[$i2][$i]) || empty($columnsAndLines[$i2][$i])) {
                    $result .= str_pad("", $columnLength, " "). $separetedBy;
                    continue;
                }

                if ($i2 === 0) {
                    $result .= $separetedBy;
                }


                // @todo isColorEncoded()
                if (strpos($columnsAndLines[$i2][$i], "\e[0m") !== false) {
                    
                    // @todo process as colored
                    
                    $line = str_replace("\e[", '', $columnsAndLines[$i2][$i]);

                    $line = str_replace("0m", '', $line);
                    
                    $explodedLine = explode('m', $line);

                    array_shift($explodedLine);
                        
                    $line = implode($explodedLine, 'm');
                    
                    $additionalColumnLength = strlen($columnsAndLines[$i2][$i]) - strlen($line);
                    
                    $result .= str_pad($columnsAndLines[$i2][$i], $columnLength + $additionalColumnLength, " ") . $separetedBy;
                } else {
                    // @todo processAsUsual
                    $result .= str_pad($columnsAndLines[$i2][$i], $columnLength, " ") . $separetedBy;
                }



                //var_dump($result);

            }

        }

        return self::processResult($result, $return);
    }


//https://joshtronic.com/2013/09/02/how-to-use-colors-in-command-line-output/

    private static function processResult(string $result, bool $return)
    {
        if ($return) {
            return $result;
        }

        echo $result;
    }

    public static function _label(
        string $msg,
        $style = TS::BOLD, $foregroundColor = FC::GREEN, $backgroundColor = BC::UNDERLINED
    )
    {
        return self::label($msg, $style, $foregroundColor, $backgroundColor, true);
    }

    public static function label(
        string $msg,
        $style = TS::BOLD, $foregroundColor = FC::GREEN, $backgroundColor = BC::UNDERLINED,
        bool $return = false
    )
    {
        return self::processResult("\e[$style;$foregroundColor;{$backgroundColor}m$msg\e[0m", $return);
    }


    public static function _tab($msg)
    {
        return self::tab($msg, true);
    }

    public static function tab($msg, $return = false)
    {
        $result = [];

        foreach (explode("\n", $msg) as $line) {
            $result[] = "\t$line";
        }

        return self::processResult(implode("\n", $result), $return);
    }


    public static function filled()
    {

    }

    public static function blankLine()
    {

    }


    public static function spaced($msg)
    {
        $result = [];

        foreach (explode("\n", $msg) as $line) {
            $result[] = " $line";
        }

        echo implode("\n", $result);
    }

    public static function linebreak(int $times = 1)
    {
        self::processResult(str_repeat("\n", $times), false);
    }

    public static function lb(int $times = 1)
    {
        self::linebreak($times);
    }


    public static function lpad($msg, $length, $str = " ")
    {
        return str_pad($msg, $length, $str, STR_PAD_LEFT);
    }
}