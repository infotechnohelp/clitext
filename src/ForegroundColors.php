<?php

namespace CliText;

class ForegroundColors
{
    const BLACK = 30;
    const RED = 31;
    const GREEN = 32;
    const YELLOW = 33;
    const BLUE = 34;
    const MAGENTA = 35;
    const CYAN = 36;
    const GREY = 37;
    const WHITE = 38;
}