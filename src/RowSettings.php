<?php

namespace CliText;

class RowSettings
{
    /**
     * @var int
     */
    private $columnLength = 25;

    /**
     * @var null|string
     */
    private $columnsSeparatedBy = null;

    /**
     * @var null|string
     */
    private $borderTopString = null;

    /**
     * @var int
     */
    private $timesToExtendTopBorder = 0;

    /**
     * @var int
     */
    private $topBorderMarginLeft = 0;

    /**
     * @var null|string
     */
    private $borderBottomString = null;

    /**
     * @var int
     */
    private $timesToExtendBottomBorder = 0;

    /**
     * @var int
     */
    private $bottomBorderMarginLeft = 0;


    # GETTERS

    /**
     * @return int
     */
    public function getColumnLength(): int
    {
        return $this->columnLength;
    }

    /**
     * @return string|null
     */
    public function getColumnsSeparatedBy(): ?string
    {
        return $this->columnsSeparatedBy;
    }

    /**
     * @return string|null
     */
    public function getBorderTopString(): ?string
    {
        return $this->borderTopString;
    }

    /**
     * @return int
     */
    public function getTimesToExtendTopBorder(): int
    {
        return $this->timesToExtendTopBorder;
    }

    /**
     * @return int
     */
    public function getTopBorderMarginLeft(): int
    {
        return $this->topBorderMarginLeft;
    }

    /**
     * @return string|null
     */
    public function getBorderBottomString(): ?string
    {
        return $this->borderBottomString;
    }

    /**
     * @return int
     */
    public function getTimesToExtendBottomBorder(): int
    {
        return $this->timesToExtendBottomBorder;
    }

    /**
     * @return int
     */
    public function getBottomBorderMarginLeft(): int
    {
        return $this->bottomBorderMarginLeft;
    }


    # SETTERS

    /**
     * @param int $columnLength
     */
    public function columnLength(int $columnLength): self
    {
        $this->columnLength = $columnLength;

        return $this;
    }

    /**
     * @param string|null $columnsSeparatedBy
     */
    public function columnsSeparatedBy(?string $columnsSeparatedBy): self
    {
        $this->columnsSeparatedBy = $columnsSeparatedBy;

        return $this;
    }

    /**
     * @param string|null $borderTopString
     */
    public function borderTopString(?string $borderTopString): self
    {
        $this->borderTopString = $borderTopString;

        return $this;
    }

    /**
     * @param int $timesToExtendTopBorder
     */
    public function timesToExtendTopBorder(int $timesToExtendTopBorder): self
    {
        $this->timesToExtendTopBorder = $timesToExtendTopBorder;

        return $this;
    }

    /**
     * @param int $topBorderMarginLeft
     */
    public function topBorderMarginLeft(int $topBorderMarginLeft): self
    {
        $this->topBorderMarginLeft = $topBorderMarginLeft;

        return $this;
    }

    /**
     * @param string|null $borderBottomString
     */
    public function borderBottomString(?string $borderBottomString): self
    {
        $this->borderBottomString = $borderBottomString;

        return $this;
    }

    /**
     * @param int $timesToExtendBottomBorder
     */
    public function timesToExtendBottomBorder(int $timesToExtendBottomBorder): self
    {
        $this->timesToExtendBottomBorder = $timesToExtendBottomBorder;

        return $this;
    }

    /**
     * @param int $bottomBorderMarginLeft
     */
    public function bottomBorderMarginLeft(int $bottomBorderMarginLeft): self
    {
        $this->bottomBorderMarginLeft = $bottomBorderMarginLeft;

        return $this;
    }


}