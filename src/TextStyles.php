<?php

namespace CliText;

class TextStyles
{
    const NONE = 0;
    const BOLD = 1;
    const UNDERLINE = 4;
}